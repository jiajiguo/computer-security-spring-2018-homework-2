# -*- coding: utf-8 -*-
"""
Created on Mon Feb 05 00:26:52 2018

@author: Jiaji Guo
"""

import csv
import numpy
import scipy

result = []
with open('ciphertexts2.txt') as tsvin:
    tsvin = csv.reader(tsvin)
    for row in tsvin:
        result.append(row[0].split())
syssize = len(result)

def judge_identical(lst):
    temp =[]
    for i in lst:
        if i in temp:
            return False
        else:
            temp.append(i)
    return True

for k in range(syssize):
    lst = ''.join(result[k])
    i = 0
    spli=[]
    while i + 16 < len(lst):
        spli.append(lst[i:i+16])
        i = i + 16
    b1 = judge_identical(spli)
    if b1 == False:
        break
        
print 'from the calculation, line number =', k, 'is ECB mode'


