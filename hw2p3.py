import string
import hashlib
import random
from Crypto.Cipher import AES
from Crypto import Random
import base64
from hashlib import md5
from base64 import b64decode
from base64 import b64encode
from Crypto.Cipher import AES

def cbc_mode(msg,rnum):                                
    key = Random.new().read(AES.block_size)           
    iv = Random.new().read(AES.block_size)
    BS = 16
    sta_str = ''.join(random.sample('abcdefghijklmnopqrstuvwxyz!@#$%^&*1234567890', rnum))
    end_str = ''.join(random.sample('abcdefghijklmnopqrstuvwxyz!@#$%^&*1234567890', BS - len(msg)% BS - rnum))
    def pad(s):
        return sta_str + s + end_str
    
#    unpad = lambda s : s[:-ord(s[len(s) - 1:])]
    def unpad(s):
        return s[rnum:-(BS - len(msg)% BS - rnum)]
    
    cbc_cipher = AES.new(key, AES.MODE_CBC, IV=iv)
    cipher_text = iv + cbc_cipher.encrypt(pad(msg))
#    cbc_decipher = AES.new(key, AES.MODE_CBC, IV=cipher_text[:16])
#    decrypted_text = cbc_decipher.decrypt(cipher_text[16:])
#    print ('CBC Mode Encrypted text: ', base64.urlsafe_b64encode(cipher_text))
    return base64.urlsafe_b64encode(cipher_text)
#    print ('CBC Mode Decrypted text: ', unpad(decrypted_text))

def ecb_mode(msg):
    BLOCK_SIZE = 16
    ecb_pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * \
                chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
#    ecb_unpad = lambda s: s[:-ord(s[len(s) - 1:])]
#    key = 'eatsleepplay'
    key = ''.join(random.sample('abcdefghijklmnopqrstuvwxyz!@#$%^&*1234567890',16)).replace(' ','')
    key = md5(key.encode('utf8')).hexdigest() 
    msg = ecb_pad(msg)
    cipher = AES.new(key, AES.MODE_ECB)
    return b64encode(cipher.encrypt(msg))

m2 ='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'

def run_encr(msg):
    r1 = random.random()
    if r1 >=0.5:
        encr_msg = ecb_mode(msg)
    else:
        r2 = random.randint(1, 15)
        encr_msg = cbc_mode(msg, r2)
    print encr_msg
    return encr_msg

for i in range(30):
    encr = run_encr(m2)
    sta = encr[0:64]
    if encr[64:128] == sta:
        print 'ECB mode'
    else:
        print 'CBC mode'
        
        